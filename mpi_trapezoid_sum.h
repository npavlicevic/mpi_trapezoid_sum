#ifndef MPI_TRAPEZOID_SUM_H
#define MPI_TRAPEZOID_SUM_H
#include <stdio.h>
#include <mpi.h>
float trapezoid(float local_a, float local_b, int local_n, float h, float (*func)(float x));
void trapezoid_data(float *a, float *b, int *n, int my_rank);
float trapezoid_square_func(float x);
/*
  trapezoid area
  1/2h[f(x1) + f(x2)]

  1/2h[f(x1) + f(x2)] + 1/2h[f(x2) + f(x3)] + 1/2h[f(x3) + f(x4)] + ... + 1/2h[f(xn-1) + f(xn)]
  1/2h[f(x1) + 2f(x2) + 2f(x3) + 2f(x4) + f(xn)]
  h[1/2f(x1) + 1/2f(xn) + f(x2) + f(x3) + f(x4)]
*/
#endif
