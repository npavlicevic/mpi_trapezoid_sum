#include "mpi_trapezoid_sum.h"

main(int argc, char **argv) {
  int my_rank;
  // the rank of current process
  int p;
  // number of processes
  float a = 0.0;
  float b = 1.0;
  int n = 1024;
  float h;
  float local_a;
  float local_b;
  int local_n;
  float area;
  float total;
  int source;
  int dest = 0;
  // send all results to 0
  int tag = 0;
  MPI_Status status;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &p);
  trapezoid_data(&a, &b, &n, my_rank);
  h = (b - a) / n;
  local_n = n / p;
  local_a = a + my_rank * local_n * h;
  local_b = local_a + local_n * h;
  area = trapezoid(local_a, local_b, local_n, h, trapezoid_square_func);
  MPI_Reduce(&area, &total, 1, MPI_FLOAT, MPI_SUM, 0, MPI_COMM_WORLD);
  if(!my_rank) {
    printf("with n = %d trapezoids, our estimate\n", n);
    printf("of the integral from %f to %f = %f\n", a, b, total);
  }
  MPI_Finalize();
}
