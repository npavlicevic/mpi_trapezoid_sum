#include "mpi_trapezoid_sum.h"

float trapezoid(float local_a, float local_b, int local_n, float h, float (*func)(float x)) {
  float area;
  float x;
  int i;
  area = (func(local_a) + func(local_b)) / 2.0;
  x = local_a;
  for(i = 1; i <= local_n - 1; i++) {
    x += h;
    area += func(x);
  }
  area *= h;
  return area;
}
void trapezoid_data(float *a, float *b, int *n, int my_rank) {
  if(!my_rank) {
    fscanf(stdin, "%f", a);
    fscanf(stdin, "%f", b);
    fscanf(stdin, "%d", n);
  }
  MPI_Bcast(a, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
  MPI_Bcast(b, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
  MPI_Bcast(n, 1, MPI_INT, 0, MPI_COMM_WORLD);
}
float trapezoid_square_func(float x) {
  return x * x;
}
